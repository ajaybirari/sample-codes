import { SampeAppPage } from './app.po';

describe('sampe-app App', () => {
  let page: SampeAppPage;

  beforeEach(() => {
    page = new SampeAppPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
