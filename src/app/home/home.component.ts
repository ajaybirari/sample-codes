import { Component } from '@angular/core';
import { AuthenticateService } from '../login/login.service';

@Component({
    selector: 'home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.css'],
    providers: [AuthenticateService]
})

export class HomeComponent {
    constructor(private authenticateService: AuthenticateService) {
    }

    logout(): void {
        this.authenticateService.logout()
    }
}