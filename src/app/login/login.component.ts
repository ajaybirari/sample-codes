import { Component } from '@angular/core';
import { LoginModel } from './login.model';
import { AuthenticateService } from './login.service';

@Component({
    selector: 'login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css'],
    providers: [AuthenticateService]
})

export class LoginComponent {
    private loginModel: LoginModel;
    private errorMessage: string;

    constructor(private authenticateService: AuthenticateService) {
        this.loginModel = new LoginModel('', '');
    }

    login(): void {
        if (!this.authenticateService.authenticate(this.loginModel)) {
            this.errorMessage = 'Failed to login';
        }
    }
}