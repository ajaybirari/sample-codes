import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Http, RequestOptions, Response, Headers } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { LoginModel } from './login.model';


@Injectable()
export class AuthenticateService {
    private authenticationUrl: string;

    private loginUsers: Array<LoginModel> = new Array<LoginModel>();


    constructor(private http: Http, private router: Router) {
        this.authenticationUrl = 'asset/content.json';

        this.loginUsers.push(new LoginModel('ajayb@gmail.com', 'pass'));
        this.loginUsers.push(new LoginModel('abc@gmail.com', 'pass'));
    }


    public logout(): any {
        localStorage.removeItem("user");
        this.router.navigate(['login']);
    }

    public authenticate(loginModel: LoginModel): any {
        let authenticatedUser = this.loginUsers.find(x => x.Username === loginModel.Username);
        if (authenticatedUser && authenticatedUser.Password === loginModel.Password) {
            localStorage.setItem("user", JSON.stringify(authenticatedUser));
            this.router.navigate(['home']);
            return true;
        } else {
            return false;
        }
    }
}
