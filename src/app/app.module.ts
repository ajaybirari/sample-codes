import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';

import { AppRouting } from './app.routing';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { AboutUsComponent } from './aboutus/aboutus.component';
import { ProfileComponent } from './profile/profile.component';
import { TeamComponent } from './team/team.component';
import { ContactUsComponent } from './contactus/contactus.component';

@NgModule({
  imports: [
    BrowserModule
    , FormsModule
    , HttpModule
    , AppRouting
  ],
  declarations: [
    AppComponent
    , LoginComponent
    , HomeComponent
    , AboutUsComponent
    , ProfileComponent
    , TeamComponent
    , ContactUsComponent
  ],
  bootstrap: [
    AppComponent
  ]
})
export class AppModule { }
