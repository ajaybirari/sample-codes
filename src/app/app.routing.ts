import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { AboutUsComponent } from './aboutus/aboutus.component';
import { ProfileComponent } from './profile/profile.component';
import { TeamComponent } from './team/team.component';
import { ContactUsComponent } from './contactus/contactus.component';

const routes: Routes = [
      {
            path: '',
            redirectTo: '/login',
            pathMatch: 'full'
      },
      {
            path: 'login',
            component: LoginComponent
      },
      {
            path: 'home',
            component: HomeComponent
      },
      {
            path: 'aboutus',
            component: AboutUsComponent,
            children: [
                  {
                        path: 'profile',
                        component: ProfileComponent
                  },
                  {
                        path: 'team',
                        component: TeamComponent
                  },
                  {
                        path: 'contactus',
                        component: ContactUsComponent
                  }
            ]
      },
      {
            path: '**',
            redirectTo: '/login'
      }
];

@NgModule({
      imports: [RouterModule.forRoot(routes)],
      exports: [RouterModule]
})
export class AppRouting {

}